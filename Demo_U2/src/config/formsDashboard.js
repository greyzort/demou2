  export const schemaHobby = [
    {
      fieldType: "TextInput",
      placeholder: "Name",
      name: "name",
      type: 'text'
    }
  ]

  export const schemaUser=
  [
    {
      fieldType: "TextInput",
      placeholder: "Name",
      label: "Name",
      name: "name",
      type: 'text'
    },
    {
      fieldType: "TextInput",
      placeholder: "Username",
      label: "Username",
      name: "username",
      type: 'text'
    },
    {
      fieldType: "TextInput",
      placeholder: "Password",
      label: "Password",
      name: "password",
      type: 'password'
    },
    {
      fieldType: "TextInput",
      placeholder: "Confirm Password",
      label: "Confirm Password",
      name: "password_confirmation",
      type: 'password'
    },
    {
      fieldType: "TextInput",
      placeholder: "Email",
      label: "Email",
      name: "email",
      type: 'text'
    },
    {
      fieldType: "TextInput",
      placeholder: "City",
      label: "City",
      name: "city",
      type: 'text'
    },
    {
      fieldType: "SelectList",
      label: "Profile",
      name: "profile_id",
      options: [
        {
          value: 1,
          text: 'Administrador'
        },
        {
          value: 2,
          text: 'Usuario'
        }
      ]
    }
  ] 

