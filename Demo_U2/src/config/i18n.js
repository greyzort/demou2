import Vue from 'vue'
import vuexI18n from 'vuex-i18n'
import store from '../store/store'
import translationsEn from '../i18n/en'
import translationsEs from '../i18n/es'

Vue.use(vuexI18n.plugin, store)

Vue.i18n.add('en', translationsEn)
Vue.i18n.add('es', translationsEs)

export const FALLBACK_LOCALE = 'en'

Vue.i18n.fallback(FALLBACK_LOCALE)

Vue.i18n.set('en')
