import Vue from 'vue'
import VueRouter from 'vue-router'
import CreateAccount from '../pages/Create-account.vue'
import Login from '../pages/Login.vue'
import Users from '../components/DashboardSections/Users'
import Hobbies from '../components/DashboardSections/Hobbies'

Vue.use(VueRouter)

const routerConfig = [
  {
    path:'/',
    name: '',
    component: Login
  },
  {
    path:'/create-account',
    name: 'CreateAccount',
    component: CreateAccount
  },
  {
    path:'/login',
    name: 'Login',
    component: Login
  },
  {
    path:'/users',
    name: 'Users',
    component: Users
  },
  {
    path:'/hobbies',
    name: 'Hobbies',
    component: Hobbies
  }
  ]

const router = new VueRouter({
  routes: routerConfig,
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

export default router;
