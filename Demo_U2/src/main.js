import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import VueCookie from 'vue-cookie'
import VueResource from 'vue-resource'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './config/i18n'
import store from './store/store.js';
import Carousel3d from 'vue-carousel-3d';
import Loading from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/vue-loading.css'


Vue.config.productionTip = false

Vue.use(Carousel3d);
Vue.use(BootstrapVue)
Vue.use(VueResource)
Vue.use(VueCookie)
Vue.use(Loading, {
  loader: 'dots',
  container: this.fullPage,
  canCancel: false,
  color: '#EC8409'
})
Vue.http.options.root = process.env.API_URL
Vue.config.lang = VueCookie.get('locale') || 'en'
Vue.http.interceptors.push((request, next) => {
  let token= localStorage.getItem('user-token');
  if(token) {
    request.headers.set('Authorization',`Bearer ${token}` )
  }
  next()
})

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
})
