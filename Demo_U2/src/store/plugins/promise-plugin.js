/**
 * Created by apokdev on 3/6/18.
 */
const hasPromise = object => (object && object.then)
  || (Array.isArray(object) && object.some(o => o && o.then));

const createPromisePlugin = ({
  status = {
    PENDING: 'PENDING',
    SUCCESS: 'SUCCESS',
    ERROR: 'ERROR',
  },
} = {}) => (store) => {
  store.subscribe(({ type, payload }) => {
    if (hasPromise(payload)) {
      store.commit(type, { meta: status.PENDING });

      payload
        .then((response) => {
          if (response.body) {
            store.commit(type, {
              data: response.body,
              meta: status.SUCCESS,
            });
          } else {
            store.commit(type, {
              data: response,
              meta: status.SUCCESS,
            });
          }
        })
        .catch((error) => {
          store.commit(type, {
            error,
            meta: status.ERROR,
          });
          throw error;
        });
    }
  });
};

export default createPromisePlugin;
