import Vue from 'vue'
import Vuex from 'vuex'
import getters from "./getters.js";
import mutations from "./mutations.js";
import actions from "./actions.js";
import createPromisePlugin from './plugins/promise-plugin';

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    loading: false,
    errors: [],
    variant: 'danger'
  },
  getters,
  mutations,
  actions,
  plugins: [
  createPromisePlugin(),
],
})
export default store
