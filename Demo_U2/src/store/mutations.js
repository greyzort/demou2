import Vue from 'vue';

const mutations = {
  getErrors(state, {item, variant}){
    state.errors = item
    state.variant = variant
  }

}

export default mutations
