import home from './es/home'
import navbar from './es/navbar'
import create from './es/create'
import login from './es/login'
import footer from './es/footer'

export default {
  home,
  navbar,
  create,
  login,
  footer
}
