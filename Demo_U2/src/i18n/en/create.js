export default {
  form:{
    name: 'Name',
    username:'Username',
    email: 'Email',
    password: 'Password',
    confirmation: 'Password Confirmation',
    city: 'City'
  },
  formTitle:{
    title: 'Register is very simple',
    subtitle:'Ingresa tus datos y disfruta todos los servicios que ofrecemos para tu negocio.'
  },
  step:{
    step1: 'Register',
    step2: 'Verification',
    step3: 'Data',
    step4: 'Pay'
  },
  verification:{
    title: 'Verify your account',
    subtitle:'Ingresa tus datos y disfruta todos los servicios que ofrecemos para tu negocio.',
    warning:'Si no tienes correo, no olvides revisar tu carpeta de SPAM.'
  },
  register:{
    onSelect:'Select your plan',
      form1:{
      title:'Completa los datos de tu empresa',
        subtitle: 'Llena el formulario para continuar.',
        name: 'Company Name',
        currency: 'Currency',
        language: 'Language',
        email: 'Email',
        cycle: 'Cycle',
        category: 'Category',
        domain: 'You have a domain:',
        location:'Location'
    },
    form2:{
      title:'Datos para la factura',
      subtitle: 'Llena los datos para realizar tu factura personalizada.',
      legal_name: 'Legal Name',
      legal_id: 'RIF',
      address: 'Address',
      phone: 'Phone'
    }
  },
  pay:{
    title:'Confirma y Paga',
    subtitle: 'Revisa si es el plan y luego selecciona tu forma de pago. ',
    form:{
      title:'Forma de Pago',
      type:{
        paypal:{},
        credit:{
          name:'Name',
          cardNumber:'Card Number',
          cvc:'CVC',
          expirationDate:'Expiration Date'},
        otra:{}
      },
      placeholder:{
        month:'Month',
        year:'Year'
      }
    },
    back:'Back to plans'
  }
}
