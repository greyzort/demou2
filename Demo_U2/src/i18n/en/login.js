export default {
  form:{
    email: 'Email',
    password: 'Password'
  },
  formTitle:{
    title: 'Iniciar Sesión',
    subtitle:'Si ya tienes una cuenta con nosotros ingresa para ver detalles de tu plan'
  },
}
