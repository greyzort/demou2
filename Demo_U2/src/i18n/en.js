import home from './en/home'
import navbar from './en/navbar'
import create from './en/create'
import login from './en/login'
import footer from './en/footer'

export default {
  home,
  navbar,
  create,
  login,
  footer
}
