export default {
  home:'DEMO US',
  characteristics: 'Caracteristicas',
  plans: 'Planes',
  documentation: 'Documentación',
  download: 'Descargar',
  register: 'Registrarse',
  login:'Soy cliente',
  logout: 'Salir'
}
