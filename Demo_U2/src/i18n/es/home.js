export default {
  proposal:{
    frasePrincipal: 'Propuesta de valor',
    fraseSecundaria: 'Fidelización de clientes'
  },
  solutions: {
    section: 'Soluciones',
    title: 'Title Solutions',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue eros neque, condimentum ornare diam aliquet eu. ',
    contenido1:
      {
        title: 'Comunicación Directa',
        description: 'Descripción solución: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue eros neque.'
      },
    contenido2: {
      title: 'Comunicación Directa',
      description: 'Descripción solución: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue eros neque.'
    },
    contenido3: {
      title: 'Comunicación Directa',
      description: 'Descripción solución: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue eros neque.'
    },
    contenido4: {
      title: 'Comunicación Directa',
      description: 'Descripción solución: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue eros neque.'
    }
  },
  services:{
    section: 'Caracteristicas',
    title: 'Tu cliente lo desea',
    labelBoton:'Descubre más',
    contenido1:
      {
        title: 'Comunicación Directa',
        description: 'Descripción solución: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue eros neque.'
      },
    contenido2: {
      title: 'Citas',
      description: 'Descripción solución: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue eros neque.'
    },
    contenido3: {
      title: 'Cupones',
      description: 'Descripción solución: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue eros neque.'
    },
    contenido4: {
      title: 'Club de fidelización',
      description: 'Descripción solución: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue eros neque.'
    }
  },
  moreServices:{
    section: 'Empresas',
    title: '¡No estas solo!',
    contenido1:{
      title: 'Comunicación Directa',
      description: 'Descripción solución: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue eros neque.'
    },
    contenido2:{
      title: 'Comunicación Directa',
      description: 'Descripción solución: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue eros neque.'
    }
  },
  companies:{
    section: 'Nuestros Clientes'
  },
  plans: {
    section:'Plans',
    title: 'El plan de la empresa',
    boton:'¡Comenzar!',
    botonmas: 'Ver más planes',
    clients:'Clients',
    branchs:'Branchs',
    users:'Users'
  }
}
