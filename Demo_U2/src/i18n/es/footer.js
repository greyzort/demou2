export default {
  copyright: 'Derechos Reservados.',
  info: 'Info',
  condition: 'Condiciones',
  url_instagram:'https://www.instagram.com/servfie/',
  url_twitter:'https://twitter.com/servfie/',
  url_facebook: 'https://www.facebook.com/servfie/ ',
  url_apok: 'https://www.grupoapok.com'
}
