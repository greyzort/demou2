export default {
  form:{
    name: 'Nombre',
    username:'Nombre de Usuario',
    email: 'Correo',
    password: 'Contraseña',
    confirmation: 'Confirmar Contraseña',
    city: 'Ciudad',
  },
  formTitle:{
    title: 'Registrarse es muy simple',
    subtitle:'Ingresa tus datos y disfruta todos los servicios que ofrecemos para tu negocio.'
  },
  step:{
    step1: 'Registro',
    step2: 'Verificación',
    step3: 'Datos',
    step4: 'Pagar'
  },
  verification:{
    title: 'Verifica tu cuenta',
    subtitle:'Ingresa tus datos y disfruta todos los servicios que ofrecemos para tu negocio.',
    warning:'Si no tienes correo, no olvides revisar tu carpeta de SPAM.'
  },
  register:{
    onSelect:'Selecciona tu plan',
    form1:{
      title:'Completa los datos de tu empresa',
      subtitle: 'Llena el formulario para continuar.',
      name: 'Nombre de la Empresa',
      currency: 'Moneda',
      language: 'Lenguaje',
      email: 'Email',
      cycle: 'Cuota',
      category: 'Categoria',
      domain: 'Dominio Existente',
      location:'Location'
    },
    form2:{
      title:'Datos para la factura',
      subtitle: 'Llena los datos para realizar tu factura personalizada.',
      legal_name: 'Razón Social o Nombre Legal',
      legal_id: 'RIF',
      address: 'Dirección Fiscal',
      phone: 'Teléfono',
    }
  },
  pay:{
    title:'Confirma y Paga',
    subtitle: 'Revisa si es el plan y luego selecciona tu forma de pago. ',
    form:{
      title:'Forma de Pago',
      type:{
        paypal:{},
        credit:{
          name:'Nombre',
          cardNumber:'N° de Tarjeta',
          cvc:'CVC',
          expirationDate:' Fecha de expiración'},
        otra:{}
      },
      placeholder:{
        month:'Mes',
        year:'Año'
      },
      back:'Volver a los planes'
    }
  }
}
