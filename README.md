# Demo U2
> A Vue.js project

## Build Setup front

# install dependencies
npm install

# serve with hot reload at localhost:8001
npm run dev

## Build Setup backend

# install dependencies
composer install

# Generate jwt key
php artisan jwt:secret

# run serve
php artisan serve