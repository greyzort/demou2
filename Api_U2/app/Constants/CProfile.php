<?php
  namespace App\Constants;

  class CProfile {
    /**
    * [Admin: 1]
    * Perfil de administrador.
    */
    const ADMIN = 1;
    /**
    * [User: 2]
    * Perfil de usuario.
    */
    const USER = 2;
  }