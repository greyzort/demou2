<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Hobby;
use App\Constants\CHttpStatus;
use App\Constants\CProfile;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $users = User::getUsers();

        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'users' => $users
            ]
        ]);
    }

    /**
     * Prepara los datos para la creacion o actualizacion de un elemento
     * @param  \Illuminate\Http\Request  $request
     * @param  int $user_id
     * @return \Illuminate\Http\Response
     */
    public function prepareCreateAndUpdateData ( Request $request, $user_id = null) {
        if($user_id) {
            if ( !$user=User::find($user_id) ) {
                return response()->json([
                    'code' => CHttpStatus::NOT_FOUND,
                    'message' => 'Usuario no encontrado'
                ]);            
            }   
        }
        if( $validator_result = $this->validateData( $request, User::rules($user_id, $request->password), trans('validation') )) {
          return $validator_result;  
        }
        $user_data = [
            'name' => $request->name,
            'username' => $request->username,
            'password' => $request->password ? Hash::make($request->password) : $user->password,
            'email' => $request->email,
            'city' => $request->city,
            'profile_id' => $user_id ? $request->profile_id : CProfile::USER
        ];

        if(!$user_id){
            $user = $this->create($user_data);
        } else {
            $user = $this->update($user, $user_data, $request->hobbies);
        }



        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'user' => $user
            ]
        ]);
    }

    /**
     * Crea un Usuario.
     *
     * @param  array  $user_data
     * @return \Illuminate\Http\Response
     */
    public function create($user_data)
    {
        return  User::create($user_data);
        
    }

    /**
     * Actualiza un hobby.
     *
     * @param  User $user
     * @param  array  $user_data
     * @return \Illuminate\Http\Response
     */
    public function update($user, $user_data, $hobbies)
    {
        $user->fill($user_data);
        $user->save();
        Hobby::updateHobbies($hobbies);
        $user->hobbies = $user->hobbies;
        
        return $user;
    }

        /**
     * Elimina un usuario.
     *
     * @param  int  $user_id
     * @return \Illuminate\Http\Response
     */
    public function delete($user_id)
    {
        //
    }
}
