<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hobby;
use App\Constants\CHttpStatus;

class HobbyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myHobbies(Request $reques)
    {
        $hobbies = auth()->user()->hobbies;
        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'hobbies' => $hobbies
            ]
        ]);
    }

    /**
     * Prepara los datos para la creacion o actualizacion de un elemento
     * @param  \Illuminate\Http\Request  $request
     * @param  int $hobby_id
     * @return \Illuminate\Http\Response
     */
    public function prepareCreateAndUpdateData(Request $request, $hobby_id=null)
    {
        if($hobby_id) {
            if ( !$hobby=Hobby::find($hobby_id) ) {
                return response()->json([
                    'code' => CHttpStatus::NOT_FOUND,
                    'message' => 'Hobby no encontrado'
                ]);            
            }   
        }

        $user_id = $request->user_id ? null : auth()->user()->id;
        
        if( $validator_result = $this->validateData( $request, Hobby::rules($user_id), trans('validation') )) {
            return $validator_result;  
        }

        $hobby_data = [
            'name' => $request->name,
            'user_id' => $user_id ? $user_id : $request->user_id
        ];
        
        if(!$hobby_id){
            $hobby = $this->create($hobby_data);
        } else {
            $hobby = $this->update($hobby, $hobby_data);
        }

        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'hobby' => $hobby
            ]
        ]);
    }

    /**
     * Crea un Hobby.
     *
     * @param  array  $hobby_data
     * @return \Illuminate\Http\Response
     */
    public function create($hobby_data)
    {
        return  Hobby::create($hobby_data);
    }

    /**
     * Actualiza un hobby.
     *
     * @param  Hobby $hobby
     * @param  array  $hobby_data
     * @return \Illuminate\Http\Response
     */
    public function update($hobby, $hobby_data)
    {
        $hobby->fill($hobby_data)->save();
        return $hobby;
    }

    /**
     * Elimina un hobby.
     *
     * @param  int  $hobby_id
     * @return \Illuminate\Http\Response
     */
    public function delete($hobby_id)
    {
        if($hobby_id) {
            if ( !$hobby=Hobby::find($hobby_id) ) {
                return response()->json([
                    'code' => CHttpStatus::NOT_FOUND,
                    'message' => 'Hobby no encontrado'
                ]);            
            }   
        }

        $hobby->delete();    

        return response()->json([
            'code' => CHttpStatus::OK,
        ]);
    }
}
