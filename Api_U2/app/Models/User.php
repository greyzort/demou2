<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'username',
        'password',
        'email', 
        'city',
        'profile_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function rules($user_id, $password) 
    {
        return [
            'name' => 'required|string|min:4|max:50', 
            'username' => [
                'required',
                'string',
                'min:4',
                Rule::unique('users')->ignore($user_id)
            ],
            'password' => !$user_id || $password ? 'required|string|min:4|max:30|confirmed' : '',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user_id)
            ], 
            'city' => 'required|string|min:4|max:30',
            'profile_id' => 'numeric'. is_null($user_id) ?'':'|required'
        ];
    }

    public Static function getUsers () {
        $users = User::with('hobbies')
        ->get();

        return $users;
    }

    public function profile()
    {
        return $this->belongsTo('App\Models\Profile');
    }
    
    public function hobbies()
    {
        return $this->hasMany('App\Models\Hobby');
    }
}
