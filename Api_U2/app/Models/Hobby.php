<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hobby extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'user_id'
    ];

    public static function rules($user_id) 
    {
        return [
            'name' => 'required|string|min:4|max:30', 
            'user_id' => is_null($user_id) ? 'required|numeric' : ''
        ];
    }

    public static function updateHobbies($hobbies) {
        foreach ($hobbies as $new_hobbie_data) {
            if($old_hobbie = Hobby::find($new_hobbie_data['id'])) {
                $old_hobbie->fill($new_hobbie_data)->save();
            }
        }
    }
}
