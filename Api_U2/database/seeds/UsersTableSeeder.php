<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Constants\CProfile;

class UsersTableSeeder extends Seeder
{
    private function getData(){
        return [
            [
                'name'          => 'administrator',
                'username'      => 'admin',
                'password'      => Hash::make('12345678'),
                'email'         => 'admin@u2.com.co',
                'city'          => 'Bogota',
                'profile_id'    => CProfile::ADMIN
            ]
        ];
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->getData() as $data){
            User::create($data);
        }    
    }
}
