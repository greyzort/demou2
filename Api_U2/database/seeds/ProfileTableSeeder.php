<?php

use Illuminate\Database\Seeder;
use App\Models\Profile;

class ProfileTableSeeder extends Seeder
{
    private function getData(){
        return [
            [
                'name'          => 'Admin',
                'description'   => 'User with all permission'
            ],
            [
                'name'          => 'User',
                'description'   => 'User with only create hobby permission'
            ]
        ];
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->getData() as $data){
            Profile::create($data);
        }    
    }
}
